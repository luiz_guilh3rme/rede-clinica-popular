<?php 
// Template Name: Home
include('includes/header.php'); 
?>

<main id="main">
    <div class="main-container position-relative">
        <div class="swiper-container" id="main-slider">
            <ul class="swiper-wrapper">           

            <?php if(have_rows('slider')) :  while (have_rows('slider')) :  the_row(); 
                $image = get_sub_field('slider-bg');
        
                if(!empty($image)):
                    $size = 'background-slider-home';
                    $thumb = $image['sizes'][$size];
                else:
                    $thumb = "https://via.placeholder.com/1500x850";
                endif;   
            ?>
            
                <li class="swiper-slide" style="background-image: url(<?= $thumb ?>)">
                    <div class="max-container">    
                    <?php if(get_sub_field("slider-link")): ?>                
                        <a href="<?php the_sub_field('slider-link'); ?>" class="main-links <?php the_sub_field('slider-title-position')?>">
                    <?php endif; ?>
                            <h2 class="main-title"><?php the_sub_field('slider-title'); ?></h2>
                    <?php if(get_sub_field("slider-link")): ?>      
                        </a>
                    <?php endif; ?>
                    </div>
                </li>
            <?php  endwhile;  endif;  ?>

            </ul>            
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>  
            <div class="swiper-pagination"></div>          
        </div>         
    </div>
</main> 

<section class="container" id="about">
    <div class="row">
        <div class="col-lg-6 col-12 about-img">
            <p class="section-description wow fadeInDown">Nós acreditamos que viver uma vida saudável é um direito de todos, sem exceção.  E é esse ideal que nos move. É ele que nos guia para oferecermos um atendimento impecável, de alta qualidade, oferecido por profissionais que realmente se importam com o seu bem-estar.</p>
        </div>

        <div class="col-lg-6 col-12 about-description wow fadeInRight">
            <h1 class="section-title-lg">São mais de 30 especialidades médicas</h1>
            <h2 class="section-title">Da estética ao check-up</h2>
            <h3 class="section-description">Nossa clínica foi projetada para colocar sempre o ser humano em primeiro lugar. Com a agilidade que só uma agenda flexível oferece à facilidade no pagamento com um preço popular, tudo o que fazemos é para atender às suas necessidades.</h3>

            <ul class="nav nav-tabs" id="about-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="tab-mission" data-toggle="tab" href="#mission" role="tab" aria-controls="mission" aria-selected="true">Missão</a>
                </li>                
                <li class="nav-item">
                    <a class="nav-link" id="tab-vision" data-toggle="tab" href="#vision" role="tab" aria-controls="vision" aria-selected="false">Visão</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="tab-values" data-toggle="tab" href="#values" role="tab" aria-controls="values" aria-selected="false">Valores</a>
                </li>
            </ul>
            <div class="tab-content" id="about-tabs-content">
                <div class="tab-pane fade show active" id="mission" role="tabpanel" aria-labelledby="tab-mission">
                    <p class="section-description icon-check"></i> Oferecer atendimento de qualidade, focando sempre no ser humano e suas necessidades, tratando todos com respeito e igualdade.</p>
                </div>                
                <div class="tab-pane fade" id="vision" role="tabpanel" aria-labelledby="tab-vision">
                    <p class="section-description icon-check"></i> Nosso objetivo é satisfazer todos aqueles que precisam de nossos serviços sendo referencia de qualidade e preço baixo em Taboão da Serra.</p>
                </div>
                <div class="tab-pane fade" id="values" role="tabpanel" aria-labelledby="tab-values">
                    <p class="section-description icon-check"></i> A Rede Clínica Popular tem como prioridade a saúde de todos. Acredita que esse direito é primordial e, por isso, oferecemos acesso a serviços de qualidade.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section  id="speciality">
    <div class="container">
        <div class="row speciality-container bg-white">
            <div class="col">

                <h2 class="section-title text-center wow fadeInDown" data-wow-duration="1.5s">Médicos altamente capacitados nas mais diversas especialidades</h2>

                <ul class="list-unstyled speciality-list">

                    <?php if(have_rows('list-specialities')) :  while (have_rows('list-specialities')) :  the_row(); 
                        $image = get_sub_field('specialities-img');
                
                        if(!empty($image)):
                            $size = 'thumbnail';
                            $thumb = $image['sizes'][$size];
                        else:
                            $thumb = "https://via.placeholder.com/150x150";
                        endif;   
                    ?>
                    
                        <li class="speciality-item wow fadeInDown">
                        <?php if(get_sub_field("specialities-link")): ?>
                            <a href="" title="<?php the_sub_field("specialities-title")?>">
                        <?php  endif; ?>
                                <img src="<?= $thumb; ?>" alt="<?php the_sub_field("specialities-title")?>" title="<?php the_sub_field("specialities-title")?>" class="mx-auto d-block" />                                
                                <p class="speciality-title"><?php the_sub_field("specialities-title")?></p>
                        <?php if(get_sub_field("specialities-link")): ?>
                            </a>
                        <?php  endif; ?>
                        </li>
                    <?php  endwhile;  endif;  ?>
                </ul>

                <a href="<?= site_url('/especialidades'); ?>" class="btn btn-orange btn-center mt-4">Conheça todas as especialidades</a>

            </div>
        </div>
    </div>
</section>

<section id="trataments">
    <div class="container">
        <div class="row bg-white">
            <div class="col-lg-6 col-12 trataments-description">
                <h2 class="section-title wow fadeInDown">Estética avançada com a melhor infraestrutura</h2>
                <ul class="list-unstyled trataments-list wow fadeInDown" data-wow-delay=".25s">                    
                    <li class="trataments-item icon-check">Toxina Botulinica</li>
                    <li class="trataments-item icon-check">Depilação a Laser</li>
                    <li class="trataments-item icon-check">Drenagem Linfática</li>
                    <li class="trataments-item icon-check">Limpeza de Pele</li>
                    <li class="trataments-item icon-check">Massagem Anti-stress</li>
                    <li class="trataments-item icon-check">Massagem Relaxante</li>
                    <li class="trataments-item icon-check">Peeling Químico</li>
                    <li class="trataments-item icon-check">Preenchimento Facial</li>
                    <li class="trataments-item icon-check">Preenchimento para Olheiras</li>
                    <li class="trataments-item icon-check">Tratamentos Faciais a Laser</li>
                    <li class="trataments-item icon-check">Tratamentos Corporais a Laser</li>
                </ul>
                <a href="<?= site_url('/estetica')?>" class="btn btn-orange btn-center">Veja mais <i class="fa fa-angle-right"></i></a>
            </div>
            <div class="col-lg-6 col-12 trataments-img">
                <img src="<?php bloginfo("template_url"); ?>/img/home/trataments-bg.jpg" alt="Tratamento estético" title="Tratamento estético" />
            </div>
        </div>
    </div>
</section>

<section class="container" id="message">
    <div class="row">
        <div class="col">
            <h2 class="section-title text-center wow fadeInDown">Qualidade e confiança na hora de pedir seus exames e te diagnosticar</h2>
        </div>
    </div>
</section>

<?php include('includes/footer.php'); ?>