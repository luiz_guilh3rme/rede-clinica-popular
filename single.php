<?php  get_template_part('includes/header'); ?>

<main class="content-internal-page" style="background-image: url(<?php bloginfo('template_url')?>/img/bg-blog.jpg">
    <div class="container max-container">
        <div class="row">

            <div class="col-10">
                <?php get_template_part("includes/components/breadcrumb"); ?>
            </div>

            <div class="col internal-wrapper blog-list-wrapper">                
                <section class="internal-content">    
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <article class="blog-list-item">
                            <header class="post-list-header">
                                <h1 class="post-title"><?= the_title(); ?></h1>
                                
                                <i class="far fa-calendar-alt mr-1"></i> <span class="post-infos"><?= ucwords(get_the_date()) ?></span>
                                <i class="fa fa-user mr-1 ml-3"></i> <span class="post-infos">Postado por <strong><?= ucwords(get_the_author()); ?></strong></span>
                            </header>
                                                        
                            <img src="<?php the_post_thumbnail_url("blog-page"); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />

                            <div class="post-list-content">
                                <?php the_content(); ?>
                            </div>

                            <footer class="post-list-footer">     
                                <div class="social-icons">
                                    <a href="#"><i class="fab fa-facebook-square"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-google-plus-g"></i></a>
                                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                                    <a href="#"><i class="far fa-envelope"></i></a>
                                </div>                                
                            </footer>
                        </article>
                    <?php endwhile; else: ?>
                        <p><?php _e('Desculpe, não encontramos esta página. :/'); ?></p>
                    <?php endif; ?>

                </section> 
                <aside class="col aside-wrapper mt-form-questions">                    
                    <?php get_template_part("includes/components/form-questions"); ?>
                </aside>
            </div>
        </div>
    </div>
</main>


<?php get_template_part('includes/footer'); ?>