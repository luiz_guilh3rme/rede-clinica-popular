var navbarContainer = $(".container-menu");
var logoDesktop =  $("#logo-rede-clinica");
var urlLogoDesktop =  logoDesktop.attr("src");
var urlLogoScroll =  logoDesktop.attr("data-logoscroll");
var internalMenu = navbarContainer.hasClass("internal-menu");

function navbarScroll() {
    navbarContainer.addClass("scroll");        
    logoDesktop.attr("src", urlLogoScroll);
        // navbarContainer.addClass("translate-y");
}

function navbarDefault() {
    navbarContainer.removeClass("scroll");    
    // navbarContainer.removeClass("translate-y");
	logoDesktop.attr("src", urlLogoDesktop);
}
$(".nav-item.dropdown").mouseenter(function(){
    $(this).addClass("show");
    $(this).children(".dropdown-menu").addClass("show");
});
$(".nav-item.dropdown").mouseleave(function(){
    $(this).removeClass("show");
    $(this).children(".dropdown-menu").removeClass("show");
});

$(window).scroll(function () {
    var browserWidth= $(window).width();

    if(browserWidth >= 1160){
        var heightScroll = $(this).scrollTop();  
        var heightScrollMenuFixed = $(window).height() * .1;   
        
        //   if (heightScroll <= heightScrollMenuFixed && !internalMenu) {
        if (heightScroll <= heightScrollMenuFixed && !internalMenu) {
            navbarDefault();            
        } else {
            navbarScroll();            
        }
    }  

});

// function verifyBrowserWidth(){
//     var browserWidth= $(window).width();

//     if(browserWidth <= 992){
//         navbarScroll();
//     }
// }
// verifyBrowserWidth();

//// menu mobile ////
var btnMenuMobile = $("#btn-menu");
var submenu = $(".nav-menu-item.has-submenu, .menu-item-has-children");
var windowWidth = $(window).width();

// menu
function openCloseMenuMobile(){        
    btnMenuMobile.click(function(){        
        navbarContainer.toggleClass("menu-active");        
        // var navbarMobile = $(".nav-menu-header, #menu-header");
        var navbarMobile = $(".container-menu-header");
        
        
            navbarMobile.toggleClass("is-open");
    });
}
openCloseMenuMobile();

function preventDefaultSubmenuHeader(){
    $(".nav-menu-item.has-submenu .nav-menu-link, .menu-item.has-submenu .menu-link").on("click", function(e){
        e.preventDefault();
    });
}
preventDefaultSubmenuHeader(); 

// submenu
var btnCloseSubmenu = $(".container-menu-header .btn-voltar");

function openCloseSubmenu(eventTypeDesktop, eventTypeMobile){  
    var eventType = windowWidth > 992 ? eventTypeDesktop : eventTypeMobile;

    submenu.on(eventType, function(){  
        btnCloseSubmenu.toggleClass("visible");
        
        if(windowWidth <= 992){
            if($(this).children(".nav-menu-submenu, .submenu").hasClass("is-open")){
                $(this).children(".nav-menu-submenu, .submenu").removeClass("is-open");
            } else {
                $(this).children(".nav-menu-submenu, .submenu").addClass("is-open");
            }
        } else {                        
            if($(this).children(".nav-menu-submenu, .submenu").hasClass("visible")){
                $(".nav-menu-submenu, .submenu").removeClass("visible");
                $(this).children(".nav-menu-submenu, .submenu").removeClass("visible");
            } else {
                $(".nav-menu-submenu, .submenu").removeClass("visible");
                $(this).children(".nav-menu-submenu, .submenu").addClass("visible");                
            }
        }                
    });    
}
openCloseSubmenu("mouseenter mouseleave", "click");



btnCloseSubmenu.on("click", function(){
    $(this).toggleClass("visible");
    $(".submenu.is-open").toggleClass("is-open");
});

$(".container-menu-header .menu-link[href='#']").on("click", function(e){
    e.preventDefault();
});

$(".container-menu-header .menu-item-has-children.menu-item-depth-0 > a").append(" <i class='fas fa-chevron-down'></i>");

btnMenuMobile.on("click", function(){
    $(this).toggleClass("is-active");
});

//- Swiper slider
var mainSlider = "#main-slider";
var swiperMain = new Swiper(mainSlider, {
    slidesPerView: 1,
	spaceBetween: 15,
	speed: 800,	
    effect: 'fade',
	fadeEffect: {
		crossFade: true
	},	
    pagination: {
    el: mainSlider + ' .swiper-pagination',
        clickable: true
    },
    navigation: {
        nextEl: mainSlider + ' .swiper-button-next',
        prevEl: mainSlider + ' .swiper-button-prev',
    }
});

// change select form schedule
var selectEspecialidade = $("#schedule-speciality");
var selectTipo = $("#schedule-type");
var labelEspecialidade = $("#speciality-label");

var especialidades = [
    "Acupuntura", "Cardiologia", "Clínico Geral", "Dermatologia", "Endocrinologista", "Escleroterapia", "Fonoaudiologia", "Gastroenterologia", "Geriatria", "Ginecologia", "Medicina Ocupacional", "Neurologia", "Nutrição", "Obstetrícia", "Oftalmologia", "Ortopedia", "Otorrinolaringologia", "Pediatria", "Pequenas Cirurgias", "Psicologia", "Psiquiatria", "Urologia", "Vascular", "Terapia Sexual"
], 
exames = [
    "Audiometria/Impedância","Bioimpedância","Colposcopia","Doppler","Ecocardiograma","Eco Fetal","Eletrocardiograma","Eletroencefalograma","Espirometria","Exames Laboratoriais","Exames Ocupacionais","Mapeamento de Retina","Nasofribrolaringoscopia","Papanicolau","Teste Ergométrico","Ultrassonografias","Vulvoscopia"
],
estetica = [
    "Depilação a Laser","Drenagem Linfática","Limpeza de Pele","Massagem Anti-stress","Massagem Relaxante","Peeling Químico","Preenchimento Facial","Preenchimento para Olheiras","Toxina Botulínica","Tratamentos Faciais a Laser","Tratamentos Corporais a Laser"
]


selectEspecialidade.on("change", function(){
    var tipoEspecialidade = $(this).val();
    var tipo = [];    

    labelEspecialidade.html(tipoEspecialidade+":*");
    
    if(tipoEspecialidade == "Especialidades") tipo = especialidades;
    else if(tipoEspecialidade == "Estética") tipo = estetica;
    else if(tipoEspecialidade == "Exames") tipo = exames;
    
    createOption(tipo);
});


function createOption(options){    
    selectTipo.html("");
    options.forEach(function(item){
        selectTipo.append("<option value='"+ item +"'>"+ item +"</option>");
    });    
}

$(".btn-whatsapp-mobile").on("click", function(e){
    e.preventDefault();
})

// Go to top
$('#go-top').click(function(){
    $('html, body').animate({scrollTop : 0},900);
    return false;
 });

 // Loader
 window.onload = function() {
    $(".container-loader").removeClass("active");
    $("body").removeClass("no-scroll"); 
 }

 // Wow.js
 new WOW().init();