function WrapperCtaVisible() {
    $(".call-cta-wrapper").addClass("visible");
    $(".call-cta-wrapper").css({
      "opacity": "1",
      "visibility": "visible"
    });
  }
  function WrapperCtaInvisible() {
    $(".call-cta-wrapper").removeClass("visible");
    $(".call-cta-wrapper").css({
      "opacity": "0",
      "visibility": "hidden"
    });
  }
  
  function handleWhatsappCta() {
      $(this).parent('.whatsapp-cta').addClass('clicked');
  }
  
  function handleCallCta() {
      $(this).addClass('clicked');
      $(".call-tooltip").addClass('clicked');
      $(".open-call-tooltip").addClass('clicked');
  }  
  function callTooltipCtaVisible(e) {
      $(".close-call-cta").removeClass('clicked');
      $(".call-tooltip").removeClass('clicked');
      $(".open-call-tooltip").removeClass('clicked');
      e.preventDefault();
  }
  
  function handleMobileCta() {
      $(this).parent('.whatsapp-tooltip').addClass('clicked');
  }
  
  function openPhoneCta() {
      // duhh
      var windowWidth = $(window).width();
      $('.open-cta').on('click', function () {
          $('.overlay').fadeIn(400, function () {   
              if(windowWidth > 992) {
                $('.form-wrapper-all.desktop').fadeIn();
              } else {
                $('.form-wrapper-all.mobile').fadeIn();
              }          
          });
  
  
          // create new date on button click
          var now = new Date(),
              days = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
              day = days[now.getDay()],
              hour = now.getHours(),
        picker = $('.form-pickers[data-instance=00]');
  
          // check if it is outside business hours
          if (day === 'Domingo' || day === 'Sábado' || hour > 18 || hour < 8) {
              picker[0].disabled = true;
              picker[0].title = "Disponível apenas em horário comercial";
              picker[0].innerHTML = "<i class='fa fa-phone'></i> APENAS  EM HORÁRIO COMERCIAL";
      }
      });
  }
  
  function formPickers() {
      $('.form-pickers').removeClass('active');
      $(this).addClass('active');
      var instance = $(this).data('instance');
  
      $('.instance:not([data-instance=' + instance + '])').fadeOut(400, function () {
          setTimeout(function () {
              $('.instance[data-instance=' + instance + ']').fadeIn()
          }, 400)
      });
  }
  
  function closePhoneCta() {
      $('.close-modal').on('click', function () {
          $('.form-wrapper-all').fadeOut(400, function () {
              $('.overlay').fadeOut();
          });
      });
  }
  
  function randomizeRequests(min, max) {
    var random = Math.floor(Math.random() * (max - min + 1)) + min;
    return random;
  }
  
  function printNumbers() {
    var numbers = document.querySelectorAll('.number');
    for (var i = 0; i < numbers.length; i++) {
        numbers[i].innerHTML = randomizeRequests(1, 12);
    }
  }

  $(window).scroll(function () {
    var heightScroll = $(this).scrollTop();  
    var heightScrollCallCta = $(window).height() * 1.1;

    if (heightScroll >= heightScrollCallCta) {
      WrapperCtaVisible();
    } else {    
      WrapperCtaInvisible();
    }
  });
  
  printNumbers();
  openPhoneCta();
  closePhoneCta();
  

  $(".form-pickers").on("click", formPickers);
  $('.close-cta').on('click', handleWhatsappCta);
  $('.close-call-cta').on('click', handleCallCta);
  $('.open-call-tooltip').on('click', callTooltipCtaVisible);
  $('.close-whatsapp-message').on('click', handleMobileCta);
  $('.btn-whatsapp-mobile').on('click', function(){ e.preventDefault() });