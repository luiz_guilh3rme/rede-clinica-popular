<?php 
// Template Name: Thankyou Page
include('includes/header.php'); 
?>
<section class="internal-img" style="background-image: url(<?php bloginfo("template_url") ?>/img/bg-thakyou-page.jpg); margin-bottom: 200px">
    <h1 class="internal-description text-uppercase mb-5">Obrigado pelo seu contato!</h1>
    <a href="<?= site_url('/')?>" class="btn btn-orange align-self-center mb-5"><i class="fa fa-chevron-left"></i> Voltar ao site</a>     
</section>
<?php

include('includes/footer.php'); 
?>