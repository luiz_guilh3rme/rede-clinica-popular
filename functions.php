<?php

// theme supports
add_theme_support('post-thumbnails'); 

if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' );
	// add_image_size( 'post-thumbnail', 290, 200, true );
	// add_image_size('blog-page', 670, 250, true);
	add_image_size('sidebar-thumbnail', 64, 64, true);
	add_image_size('background-slider-home', 1500, 850, true);
	add_image_size('background-topo', 1920, 600, true);
}

function excerpt($limit, $instance) {
	$excerpt = explode(' ', $instance, $limit);
	if ( count( $excerpt ) >= $limit ) {
		array_pop( $excerpt );
		$excerpt = implode(" ", $excerpt ).' ...';
	} 
	else {
		$excerpt = implode( " ", $excerpt );
	}	
	$excerpt = preg_replace( '`[[^]]*]`', '', $excerpt );
	return $excerpt;
}

// remove annoying margin-top 32px !important from WP admin bar, and the bar itself
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

add_action('get_header', 'remove_admin_login_header');
show_admin_bar(false);


## GENERAL CHANGES TO WP "CORE" ## 
##################################

// Add copyright to wp-admin footer

function change_footer_admin() {
	echo 'Tema desenvolvido por: <a href="https://www.3xceler.com.br" target="_blank" rel="noopener">Agência 3xceler</a> usando: <a href="http://www.wordpress.org" target="_blank" rel="noopener nofollow">WordPress</a></p>';
}
add_filter('admin_footer_text', 'change_footer_admin');

// remove welcome panel
remove_action('welcome_panel', 'wp_welcome_panel');


## menus ## 
###########

add_action('init', 'register_menus');

function register_menus() {
	register_nav_menu('Header', 'Header');
}


// menu walker
// https://developer.wordpress.org/reference/functions/wp_nav_menu/ -> comments 

class Doctor_Walker_Nav_Menu extends Walker_Nav_Menu {
	/**
	* Starts the list before the elements are added.
	*
	* Adds classes to the unordered list sub-menus.
	*
	* @param string $output Passed by reference. Used to append additional content.
	* @param int    $depth  Depth of menu item. Used for padding.
	* @param array  $args   An array of arguments. @see wp_nav_menu()
	*/

   function start_lvl( &$output, $depth = 0, $args = array() ) {

	   $indent = ( $depth > 0 ? str_repeat("\t", $depth) : '' ); 
	   $display_depth = ( $depth + 1 ); 
	   $classes = array( 
		   ( $display_depth == 1 ? 'submenu' : ''),
		   ( $display_depth == 2 ? 'third-menu' : '' ), 
		   ( $display_depth > 1 ? 'depth-' . $display_depth : '' )
	   ); 

	   $class_names = implode( ' ', $classes );

	   // output

	   $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
   }

	/**
	* Start the element output.
	*
	* Adds main/sub-classes to the list items and links.
	*
	* @param string $output Passed by reference. Used to append additional content.
	* @param object $item   Menu item data object.
	* @param int    $depth  Depth of menu item. Used for padding.
	* @param array  $args   An array of arguments. @see wp_nav_menu()
	* @param int    $id     Current item ID.
	*/

   function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
	   global $wp_query;
	   
	   $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

	   // Depth-dependent classes.
	   $depth_classes = array(
		   ( $depth == 1 ? 'submenu-link' : '' ),
		   ( $depth == 2 ? 'third-menu-link' : '' ),
		   'menu-item-depth-' . $depth
	   );
	   
	   $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

	   // Passed classes.
	   $classes = empty( $item->classes ) ? array() : (array) $item->classes;
	   $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

	   // Build HTML.
	   $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

	   // Link attributes.
	   $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
	   $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
	   $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
	   $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	   $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

	   // Build HTML output and pass through the proper filter.
	   $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
		   $args->before,
		   $attributes,
		   $args->link_before,
		   apply_filters( 'the_title', $item->title, $item->ID ),
		   $args->link_after,
		   $args->after
	   );
	   $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
   }
}