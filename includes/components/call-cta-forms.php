 <div class="overlay">
 <button class="close-modal" aria-label="Fechar Modal">&times;</button>
    <div class="form-wrapper-all desktop">
        <div class="form-picker">
            <button class="form-pickers" data-instance="00">
                <i class="fa fa-phone"></i>
                ME LIGUE AGORA
            </button>
            <button class="form-pickers" data-instance="01">
                <i class="fa fa-clock alt"></i>
                ME LIGUE DEPOIS
            </button>
            <button class="form-pickers active" data-instance="02">
                <i class="fa fa-comments alt"></i>
                DEIXE UMA MENSAGEM
            </button>
        </div>
        <div class="instance" data-instance="01">
            <!-- <form class="leave-message schedule-time" data-form="form-schedule" method="POST">
                <legend class="leave-title">
                    Gostaria de agendar e receber uma
                    chamada em outro horário?
                </legend>
                <div class="form-row">                    
                    <div class="form-group col">
                        <label class="sr-only" for="schedule-user-hour">Horário:</label>
                        <input type="time" class="form-control" id="schedule-user-hour" name="schedule-user-hour" placeholder="Horário *" required />
                    </div>       
                    <div class="form-group col">
                        <label class="sr-only" for="schedule-user-date">Dia:</label>
                        <input type="date" class="form-control" id="schedule-user-date" name="schedule-user-date" placeholder="Dia *" required />
                    </div>
                    <div class="form-group col-12">
                        <label class="sr-only" for="schedule-user-name">Seu nome:</label>
                        <input type="text" class="form-control" id="schedule-user-name" name="schedule-user-name" placeholder="Informe seu nome *" required />
                    </div>
                    <div class="form-group col-12">
                        <label class="sr-only" for="schedule-user-phone">Telefone:</label>
                        <input type="tel" class="form-control" id="schedule-user-phone" name="schedule-user-phone" placeholder="Informe seu telefone *" required />
                    </div>       
                </div>
                <button class="btn btn-orange btn-block">Me ligue depois</button>
                <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
            </form> -->
            <?php echo do_shortcode('[contact-form-7 id="199" title="Modal - Me ligue depois" html_class="leave-message"]'); ?>
        </div>
        <div class="instance active" data-instance="02">
            <!-- <form class="leave-message" method="POST" data-form="form-leave-message">
                <legend class="leave-title">
                    Deixe sua mensagem! Entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="form-row">
                    <div class="form-group col-12">
                        <label class="sr-only" for="leave-message-message">Mensagem:</label>
                        <textarea class="form-control" id="leave-message-message" name="leave-message-message" rows="4" placeholder="Deixe sua mensagem" required></textarea>
                    </div>       
                    <div class="form-group col-12">
                        <label class="sr-only" for="leave-message-user-name">Seu nome:</label>
                        <input type="text" class="form-control" id="leave-message-user-name" name="leave-message-user-name" placeholder="Seu nome *" required />
                    </div>
                    <div class="form-group col-12">
                        <label class="sr-only" for="leave-message-user-phone">Seu telefone:</label>
                        <input type="tel" class="form-control" id="leave-message-user-phone" name="leave-message-user-phone" placeholder="Informe seu telefone *" required />
                    </div>
                </div>
                <button class="btn btn-orange btn-block">Me ligue depois</button>
                <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
            </form> -->
            <?php echo do_shortcode('[contact-form-7 id="200" title="Modal - Deixe uma mensagem" html_class="leave-message"]'); ?>
        </div>
        <div class="instance" data-instance="00">        	
            <!-- <form class="leave-message" data-form="form-we-call" method="POST">
                <legend class="leave-title">
                    <span class="variant">
                        NÓS TE LIGAMOS!
                    </span>
                    Informe seu telefone que entraremos em
                    contato o mais rápido possível.
                </legend>
                <div class="form-row">
                    <div class="form-group col-12">
                        <label class="sr-only" for="we-call-user-phone">Telefone:</label>
                        <input type="tel" class="form-control" id="we-call-user-phone" name="we-call-user-phone" placeholder="Informe seu telefone *" required />
                    </div>     
                    <div class="form-group col-12">
                        <label class="sr-only" for="we-call-user-name">Seu nome:</label>
                        <input type="text" class="form-control" id="we-call-user-name" name="we-call-user-name" placeholder="Informe seu nome *" required />
                    </div>                      
                </div>
                <button class="btn btn-orange btn-block">Me ligue agora</button>
                <p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
            </form> -->
            <?php echo do_shortcode('[contact-form-7 id="201" title="Modal - Me ligue agora" html_class="leave-message"]'); ?>
        </div>
    </div>
    <div class="form-wrapper-all mobile">
        <div class="instance active">
        	<?/*
            <form class="leave-message schedule-time" data-form="form-preorder-consultation">
                <legend class="leave-title">
                    <h4 class="internal-form-title"><span class="title-thin">Pré-agende sua</span> Consulta</h4>
                </legend>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="schedule-name">Nome completo:*</label>
                        <input type="text" class="form-control" id="schedule-name" name="schedule-name" placeholder="Digite seu nome*" required />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="schedule-email">Email válido:*</label>
                        <input type="email" class="form-control" id="schedule-email" name="schedule-email" placeholder="Digite seu e-mail*" required />
                    </div>                
                </div>
                <div class="form-row">                    
                    <div class="form-group col-6">
                        <label class="sr-only" for="schedule-speciality">Opção desejada:*</label>
                        
                        <select id="schedule-speciality" name="schedule-speciality" class="form-control" required>
                            <option value="">-- Selecione</option>
                            <option value="Especialidades">Especialidades</option>
                            <option value="Estética">Estética</option>
                            <option value="Exames">Exames</option>
                        </select>
                    </div>  
                    <div class="form-group col-6">
                        <label class="sr-only" for="schedule-type" id="speciality-label">Nome da especialidade:*</label>
                        
                        <select id="schedule-type" name="schedule-type" class="form-control" required>
                            <option value="">---</option>
                        </select>
                    </div>                 
                </div>
                <div class="form-row">
                    
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="schedule-phone">DDD + Telefone:*</label>
                        <input type="tel" class="form-control" id="schedule-phone" name="schedule-phone" placeholder="DDD + Telefone*" required />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="schedule-file">Anexar arquivo:</label>
                        <input type="file" class="form-control" id="schedule-file" name="schedule-file" />
                    </div>                                 
                </div>
                <div class="form-group">
                    <label class="sr-only" for="schedule-message">Mensagem</label>
                    <textarea class="form-control" id="schedule-message" name="schedule-message" placeholder="Escreva sua mensagem*" rows="4" required></textarea>
                </div>
                <button type="submit" class="btn btn-orange">Agendar</button>
            </form> */?>
            <?php echo do_shortcode('[contact-form-7 id="205" title="Modal - Formulário de Agendamento" html_class="leave-message"]'); ?>
        </div>
    </div>
 </div>