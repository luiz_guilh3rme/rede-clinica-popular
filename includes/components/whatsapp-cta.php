<div class="whatsapp-cta">
	<a href="wpp" rel="nofollow noopener" target="_blank" title="Fale conosco no Whatsapp" class="wpp-lateral">
		<span class="whatsapp-cta-text btn btn-orange">LIGAR</span>
		<img src="<?php bloginfo("template_url")?>/img/common/whatsapp.png" alt="Ícone whatsapp" title="Envie-nos um whatsapp!" class="whatsapp-cta-img" />
	</a>
	<a href="#" class="btn btn-orange open-cta" aria-label="Abrir formulário agendar consulta">AGENDE SUA CONSULTA</a>
</div>