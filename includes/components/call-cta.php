<div class="call-cta-wrapper">
	<div class="call-tooltip">
		<p class="tooltip-text">
			<span class="variant">Olá!</span>
		Gostaria de receber uma ligação gratuita?</p>
		<button class="btn btn-blue-light open-cta confirm">SIM</button>
	</div>  
	<a href="#" class="open-call-tooltip">
		<img src="<?php bloginfo("template_url")?>/img/common/callcta.png" aria-hidden="true" class="call-cta" />
	</a>
	<button class="close-call-cta" aria-label="Fechar CTA de Ligação">
		&times;
	</button>
</div>