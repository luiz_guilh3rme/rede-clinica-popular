<h5 class="footer-title mt-50">Faça seu agendamento</h5>

<a class="footer-contact" href="tel:+551142459696"><i class="fas fa-phone"></i> (11) 4245-9696</a>

<a class="footer-contact" href="wpp"><i class="fab fa-whatsapp"></i> (11) 96863-5505</a>