<h5 class="footer-title mt-50">Onde estamos</h5>
<p class="footer-local"><i class="fas fa-map-marker-alt"></i> Rua Elizabetta Lips, nº 184</p>
<p>Jd. Bom Tempo - Taboão da Serra / SP</p>
<a href="https://www.redeclinicapopular.com.br/contato/" class="btn btn-orange mt-4">Como chegar</a>