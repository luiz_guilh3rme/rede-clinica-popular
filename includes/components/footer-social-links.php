<h5 class="footer-title mt-50">Redes sociais</h5>
<p class="pb-4">Siga-nos nas redes sociais e fique por dentro de todas as novidades.</p>
<a href="https://www.instagram.com/redeclinicapopular/" class="footer-socials-link" title="Acesse nosso Instagram" target="_blank" rel="noopener nofollow">
    <i class="fab fa-instagram"></i>
</a>
<a href="https://www.facebook.com/redeclinica" class="footer-socials-link" title="Acesse nosso Facebook" target="_blank" rel="noopener nofollow">
    <i class="fab fa-facebook-square"></i>
</a>
<a href="https://www.youtube.com/channel/UCqrmjkeCzvaxrhjg7rlGy6g?view_as=subscriber" class="footer-socials-link" title="Acesse nosso canal no Youtube" target="_blank" rel="noopener nofollow">
    <i class="fab fa-youtube"></i>
</a>
<a href="https://plus.google.com/u/0/115901906278591345607" class="footer-socials-link" title="Acesse nosso Google Plus" target="_blank" rel="noopener nofollow">
    <i class="fab fa-google-plus-square"></i>
</a>