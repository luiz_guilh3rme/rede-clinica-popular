<div class="social-networks">
    <h3 class="internal-form-title mb-5">Redes Sociais</h3>
    <a href="#" class="social-link facebook">
        <i class="fab fa-facebook-f"></i> Facebook
    </a>
    <a href="#" class="social-link twitter mt-3">
        <i class="fab fa-twitter"></i> Twitter
    </a>
    <a href="#" class="social-link instagram mt-3">
        <i class="fab fa-instagram"></i> Instagram
    </a>
    <a href="#" class="social-link whatsapp mt-3">
        <i class="fab fa-whatsapp"></i> Whatsapp
    </a>
</div>