<?php
    $template_info = get_post_meta(get_queried_object_id());
    $page_specialities = "tp-especialidades.php";
    $page_thankyou = "tp-thankyou-page.php";

    define("TEMPLATE_NAME",     $template_info["_wp_page_template"][0]);    
    define("PAGE_SPECIALITIES", $page_specialities);    
    define("PAGE_THANKYOU", $page_thankyou);    