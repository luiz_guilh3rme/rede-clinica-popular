<footer id="footer" class="position-relative">
    <div class="container">
        <div class="row center-mobile">
            <div class="col-lg-3 col-md-6 col-12">
                <?php get_template_part("includes/components/footer-logo"); ?>

                <!-- <a href="#" class="link-footer">Continue lendo</a> -->
                                
                <?php
                    get_template_part("includes/components/footer-social-links");
                    get_template_part("includes/components/footer-address");
                ?>                        
            </div>
            <nav class="col-lg-3 col-md-6 col-12 mt-md-0 mt-5">
                <h5 class="footer-title">Especialidades</h5>
                <ul class="list-nav-footer">
                    <li class="list-nav-item"><a href="<?=site_url("/acupuntura-em-taboao-da-serra/");?>" title="Aculputura em Taboão da Serra">Acupuntura</a></li>
                    <li class="list-nav-item"><a href="<?=site_url("/cardiologista-em-taboao-da-serra/");?>" title="Cardiologista em Taboão da Serra">Cardiologista</a></li>
                    <li class="list-nav-item"><a href="<?=site_url("/clinico-geral-em-taboao-da-serra/");?>" title="Clínico Geral em Taboão da Serra">Clínico Geral</a></li>
                    <li class="list-nav-item"><a href="<?=site_url("/clinico-geral-em-taboao-da-serra/");?>" title="Clínico Geral em Taboão da Serra">Dermatologista</a></li>
                    <li class="list-nav-item"><a href="#">Endocrinologista</a></li>
                    <li class="list-nav-item"><a href="#">Escleroterapia</a></li>
                    <li class="list-nav-item"><a href="#">Fonoaudiologia</a></li>
                    <li class="list-nav-item"><a href="#">Gastroenterologia</a></li>
                    <li class="list-nav-item"><a href="#">Geriatria</a></li>
                    <li class="list-nav-item"><a href="#">Ginecologia</a></li>
                    <li class="list-nav-item"><a href="#">Medicina Ocupacional</a></li>
                    <li class="list-nav-item"><a href="#">Neurologia</a></li>
                    <li class="list-nav-item"><a href="#">Nutrição</a></li>
                    <li class="list-nav-item"><a href="#">Obstetrícia</a></li>
                    <li class="list-nav-item"><a href="#">Oftalmologia</a></li>
                    <li class="list-nav-item"><a href="#">Ortopedia</a></li>
                    <li class="list-nav-item"><a href="#">Otorrinolaringologia</a></li>
                    <li class="list-nav-item"><a href="#">Pediatria</a></li>
                    <li class="list-nav-item"><a href="#">Pequenas Cirurgias</a></li>
                    <li class="list-nav-item"><a href="#">Psicologia</a></li>
                    <li class="list-nav-item"><a href="#">Psiquiatria</a></li>
                    <li class="list-nav-item"><a href="#">Urologia</a></li>
                    <li class="list-nav-item"><a href="#">Vascular</a></li>
                    <li class="list-nav-item"><a href="#">Terapia Sexual</a></li>
                </ul>
            </nav>
            <div class="col-lg-3 col-md-6 col-12 mt-lg-0 mt-5">
                <h5 class="footer-title">Exames</h5>                
                <ul class="list-nav-footer">                    
                    <li class="list-nav-item"><a href="#">Audiometria/Impedância</a></li>
                    <li class="list-nav-item"><a href="#">Bioimpedância</a></li>
                    <li class="list-nav-item"><a href="#">Colposcopia</a></li>
                    <li class="list-nav-item"><a href="#">Doppler</a></li>
                    <li class="list-nav-item"><a href="#">Ecocardiograma</a></li>
                    <li class="list-nav-item"><a href="#">Eco Fetal</a></li>
                    <li class="list-nav-item"><a href="#">Eletrocardiograma</a></li>
                    <li class="list-nav-item"><a href="#">Eletroencefalograma</a></li>
                    <li class="list-nav-item"><a href="#">Espirometria</a></li>
                    <li class="list-nav-item"><a href="#">Exames Laboratoriais</a></li>
                    <li class="list-nav-item"><a href="#">Exames Ocupacionais</a></li>
                    <li class="list-nav-item"><a href="#">Mapeamento de Retina</a></li>
                    <li class="list-nav-item"><a href="#">Nasofribrolaringoscopia</a></li>
                    <li class="list-nav-item"><a href="#">Papanicolau</a></li>
                    <li class="list-nav-item"><a href="#">Teste Ergométrico</a></li>
                    <li class="list-nav-item"><a href="#">Ultrassonografias</a></li>
                    <li class="list-nav-item"><a href="#">Vulvoscopia</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-6 col-12 mt-lg-0 mt-5">
                <h5 class="footer-title">Estética</h5>
                <ul class="list-nav-footer">
                    <li class="list-nav-item"><a href="">Depilação a Laser</a></li>
                    <li class="list-nav-item"><a href="">Drenagem Linfática</a></li>
                    <li class="list-nav-item"><a href="">Limpeza de Pele</a></li>
                    <li class="list-nav-item"><a href="">Massagem Anti-stress</a></li>
                    <li class="list-nav-item"><a href="">Microagulhamento Capilar</a></li>
                    <li class="list-nav-item"><a href="">Microagulhamento Facial</a></li>
                    <li class="list-nav-item"><a href="">Micropigmentação</a></li>
                    <li class="list-nav-item"><a href="">Massagem Relaxante</a></li>
                    <li class="list-nav-item"><a href="">Peeling Químico</a></li>
                    <li class="list-nav-item"><a href="">Preenchimento Facial</a></li>
                    <li class="list-nav-item"><a href="">Preenchimento Labial</a></li>
                    <li class="list-nav-item"><a href="">Preenchimento para Olheiras</a></li>
                    <li class="list-nav-item"><a href="">Toxina Botulínica</a></li>
                    <li class="list-nav-item"><a href="">Tratamentos Faciais a Laser</a></li>
                    <li class="list-nav-item"><a href="">Tratamentos Corporais a Laser</a></li>
                </ul>
                             
                <?php get_template_part("includes/components/footer-phones")?>
            </div>
        </div>
    </div>
    <?php get_template_part("includes/sections/footer-copyright")?>
</footer>

<?php
	get_template_part("includes/components/call-cta");
    get_template_part("includes/components/call-cta-forms");
    get_template_part("includes/components/whatsapp-cta");
    get_template_part("includes/sections/loader");
?>

<script src="<?php bloginfo("template_url"); ?>/dist/app.js"></script>
<?php wp_footer(); ?>
<script type="text/javascript" src="https://phonetrack-static.s3.sa-east-1.amazonaws.com/3a30be93eb45566a90f4e95ee72a089a.js" id="script-pht-phone" data-cookiedays="5"></script>
</body>
</html> 
