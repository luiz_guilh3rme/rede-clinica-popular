<?php get_template_part("includes/config/template-name"); ?>
<!DOCTYPE html>
<!--[if IE]><html lang="pt-br" class="lt-ie9 lt-ie8"><![endif]-->
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title><?php wp_title(); ?></title>
	<?php wp_head(); ?>
	<meta name="description" content="Nossa clínica foi projetada para colocar sempre o ser humano em primeiro lugar. Com a agilidade que só uma agenda flexível oferece à facilidade no pagamento com um preço popular, tudo o que fazemos é para atender às suas necessidades." />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="language" content="pt-br" />
	<meta name="author" content="" />
	<meta name="language" content="pt-br" />
    <link rel="canonical" href="https://<?= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] ?>" />
	<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/src/css/swiper.css" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/dist/main.css" type="text/css" />
	<meta name="theme-color" content="#ffffff" />
	<!-- <link rel="shortcut icon" href="<?= get_bloginfo('template_url') ?>/img/favicon.ico" type="image/x-icon" /> -->

	<?php if(TEMPLATE_NAME == PAGE_THANKYOU): ?>
	<meta name="robots" content="noindex" />
	<?php endif; ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-W2K66VH');</script>
	<!-- End Google Tag Manager -->
</head>
<body class="no-scroll">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W2K66VH"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<div class="oldiewarning" aria-hidden="true" hidden="true">
		<a href="https://www.google.com.br/chrome/browser/desktop/" aria-hidden="true">
			POR FAVOR, CLIQUE AQUI E ATUALIZE SEU NAVEGADOR PARA ACESSAR O SITE.
		</a>
	</div>
	
	<header id="header">		
		<!-- <div class="container-menu <?php if(!is_front_page()){ echo "scroll internal-menu"; } ?>"> -->
		<div class="container-menu">
			<nav class="nav-menu">
				<a class="nav-brand" href="<?= site_url('/'); ?>">
					<img src="<?php bloginfo("template_url"); ?>/img/shared/logo-rede-clinica-popular.png" title="Ir para a página inicial" alt="Logo Rede Clínica Popular" id="logo-rede-clinica" data-logoscroll="<?php bloginfo("template_url"); ?>/img/shared/logo-rede-clinica-popular2.png" />		
				</a>
				<div class="container-menu-header">
					<?php 
						wp_nav_menu( 
							array(
								'theme-location' => 'Header', 
								'menu' => 'Header',
								'container' => false,
								'walker' => new Doctor_Walker_Nav_Menu()
							) 
						);
					?>
					<a class="btn btn-orange btn-agende" href="<?= site_url('/agendamento'); ?>" title="Saiba mais sobre Agende Agora">Agende Agora</a>
					<a class="btn btn-orange btn-voltar" href="#" aria-label="Fechar sub-menu"><i class="fas fa-chevron-left"></i> Voltar</a>
				</div>

				<button id="btn-menu" class="hamburger hamburger--spin js-hamburger" type="button" aria-label="Abrir menu mobile">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>

			</nav>
		</nav>
	</header>
