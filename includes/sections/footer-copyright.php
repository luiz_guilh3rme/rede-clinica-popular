<div class="footer-copyright position-relative">
    <div class="container">            
        <div class="row align-items-center">
            <div class="col footer-first">            
                <p class="copyright-description">Rede Clínica Popular &copy; Todos os direitos Reservados</strong></p>
            </div>
            <div class="col footer-last">
                <a href="https://www.3xceler.com.br/otimizacao-de-sites/" target="_blank" rel="noopener" title="Otimização de Sites" class="link-3xceler">
                    <span class="sr-only">3xceler </span>
                    <p class="txt-3xceler">Otimização de Sites: </p>
                    <img src="<?php bloginfo("template_url"); ?>/img/logo-3xceler.png" alt="Logo Agência 3xceler" title="3xceler | Marketing Performance" />
                </a>
            </div>
        </div>
    </div>
    <!-- <a href="#" id="go-top"><i class="fas fa-chevron-up"></i></a> -->
</div>
