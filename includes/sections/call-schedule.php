<section class="call-schedule text-center">
    <h3 class="call-title" style="max-width: 470px">Venha nos fazer uma visita, será um prazer atendê-lo(a)!</h3>
    <a href="<?= site_url("/agendamento")?>" class="btn btn-orange" title="Agendar meu horário">Agendar meu horário</a>
</section>