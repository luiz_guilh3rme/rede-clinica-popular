<?php get_template_part("includes/sections/background-top"); ?>
<main class="container contact-container">
    <div class="row bg-white">
        <div class="col">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7311.639140313183!2d-46.759048!3d-23.610803!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x431695d8addfaa9!2sREDE+CL%C3%8DNICA+POPULAR!5e0!3m2!1spt-BR!2sbr!4v1549564443053" class="contact-map" width="100%" height="450" frameborder="0" title="Mapa Rede Clínica Popupar | R. Elizabetta Lips, 184 - Jardim Bom Tempo, Taboão da Serra - SP, 06763-190" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="row py-5">
        <div class="col-lg-5 col-12 mb-lg-0 mb-5 wow fadeInLeft">
            <h2 class="section-title">Nossos contatos</h2>
            <ul class="list-unstyled">
                <li><a href="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7311.639140313183!2d-46.759048!3d-23.610803!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x431695d8addfaa9!2sREDE+CL%C3%8DNICA+POPULAR!5e0!3m2!1spt-BR!2sbr!4v1549564443053" class="contact-link" title="Mapa Rede Clínica Popular" target="_blank" rel="nofollow noopener"><i class="fas fa-map-marker-alt"></i> Rua Elizabetta Lips, nº 184 <br/>Jd. Bom Tempo - Taboão da Serra / SP</a></li>
                <?php
                    $caracters = array("(", ")", "-", " ");
                    $phoneNumber = get_field("contact-phone");
                    $whatsappNumber = get_field("contact-whatsapp");
                ?>
                <li><a class="contact-link" href="tel:+55<?= str_replace($caracters, "", $phoneNumber) ?>"><i class="fas fa-phone"></i> <?= $phoneNumber ?></a></li>
                <li><a class="contact-link" href="tel:+55<?= str_replace($caracters, "", $whatsappNumber) ?>"><i class="fab fa-whatsapp"></i> <?= $whatsappNumber ?></a></li>
                <li><a href="mailto:<?php the_field("contact-email")?>" class="contact-link"><i class="fas fa-envelope"></i> <?php the_field("contact-email")?></a></li>
            </ul>
        </div>

        <div class="col-lg-7 col-12 bg-white contact-form wow fadeInRight">
            <?php /*
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="contact-name">Nome</label>
                        <input type="text" class="form-control" id="contact-name" name="contact-name" placeholder="Nome*" required />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="contact-email">Email</label>
                        <input type="email" class="form-control" id="contact-email" name="contact-email" placeholder="E-mail*" required />
                    </div>                
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="contact-phone">Telefone</label>
                        <input type="tel" class="form-control" id="contact-phone" name="contact-phone" placeholder="DDD + Telefone*"required />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="sr-only" for="contact-subject">Assunto</label>
                        <input type="email" class="form-control" id="contact-subject" name="contact-subject" placeholder="Assunto*" required />
                    </div>                
                </div>
                <div class="form-group">
                    <label class="sr-only" for="contact-message">Mensagem</label>
                    <textarea class="form-control" id="contact-message" name="contact-message" placeholder="Escreva sua mensagem*" rows="4" required></textarea>
                </div>
                <button type="submit" class="btn btn-orange btn-block">Enviar mensagem</button>
            </form>
            
            */?>
            <?php echo do_shortcode('[contact-form-7 id="6" title="Formulário Contato"]'); ?>
        </div>
    </div>
</main>