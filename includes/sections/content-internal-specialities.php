<main  class="internal-content page-specialities">
    <div class="container">
        <div class="container-desk-lg">
            <div class="row">
                <div class="col-lg-7 col-12 bg-white">
                    <div class="internal-container"> 
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>                   
                            <?php the_content(); ?>
                        <?php endwhile; else: ?>
                            <p><?php _e('Não há posts.'); ?></p>
                        <?php endif; ?>                    
                    </div>
                </div>
                <div class="col-lg col-12 bg-white pb-desk">
                    <?php echo do_shortcode("[contact-form-7 id='247' title='Modal - Formulário de Agendamento Página interna' html_class='bg-white form-aside']"); ?>                    
                    <!-- <img src="<?php // bloginfo("template_url")?>/img/banner-lateral.jpg" class="d-block mx-auto w-100" alt="Bannner" title="Banner" /> --> 
                </div>            
            </div>    
        </div>        
    </div>
</main>

<?php get_template_part("includes/sections/questions")?>
<?php get_template_part("includes/sections/tratament")?>