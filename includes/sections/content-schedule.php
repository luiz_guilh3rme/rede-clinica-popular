<?php get_template_part("includes/sections/background-top"); ?>
<main class="container contact-container mb-5">
    <div class="row bg-white">
        <div class="col bg-white contact-form">   
            <?php echo do_shortcode('[contact-form-7 id="198" title="Formulário Agendamento"]'); ?>
        </div>
    </div>
</main>