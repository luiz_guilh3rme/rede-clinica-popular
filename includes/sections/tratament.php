<?php if(get_field('trataments')): ?>
<section class="tratament">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                <?php the_field('trataments'); ?>
            </div>
            <?php if(get_field("trataments-map")): ?>
            <div class="col-lg col-12">
                <<!-- img src="<?php bloginfo("template_url")?>/img/tratament/mapa-taboao.jpg" alt="Mapa Taboão da Serra" title="Mapa Taboão da Serra" class="img-tratament-map" /> -->
                <?php  the_field("trataments-map"); ?>
            </div>
        	<?php endif; ?>
        </div>
    </div>
</section>
<?php endif; ?>