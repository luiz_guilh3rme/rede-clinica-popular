<main  class="internal-content">
    <div class="container">
        <div class="row">
            <div class="col">

                <div class="internal-container bg-white">                    
                    <h1 class="internal-title text-center wow fadeIn"><?php the_field("content-title"); ?></h1>

                    <?php 
                        $itens = get_field("items-by-line"); 
                        $class = $itens == 2 ? $class="two-by-line" : $class="three-by-line";
                    ?>

                    <ul class="list-unstyled internal-list">
                                                
                    <?php 
                        if(have_rows("list-itens")):  while(have_rows("list-itens")): the_row(); 
                            $image = get_sub_field('list-icon');

                            if(!empty($image)):
                                $size = 'thumbnail';
                                $image_url = $image['sizes'][$size];
                            else:
                                $image_url = "https://via.placeholder.com/80x80";
                            endif;
                    ?>

                        <li class="internal-item wow fadeInDown <?= $class; ?>">
                        <?php if(get_sub_field("list-link")): ?>
                            <a href="<?php the_sub_field("list-link") ?>" title="<?php the_sub_field("list-title"); ?>" class="internal-link">
                                <img src="<?= $image_url; ?>" alt="<?php the_sub_field("list-title"); ?>" title="<?php the_sub_field("list-title"); ?>" class="internal-icon" />
                            </a>
                            <a href="<?php the_sub_field("list-link") ?>" title="<?php the_sub_field("list-title"); ?>" class="internal-link">
                                <h3 class="internal-title"><?php the_sub_field("list-title"); ?></h3>
                                <p class="internal-description"><?php the_sub_field("list-description"); ?></p>
                            </a>
                        <?php else: ?>
                            <img src="<?= $image_url; ?>" alt="<?php the_sub_field("list-title"); ?>" title="<?php the_sub_field("list-title"); ?>" class="internal-icon" />
                            <h3 class="internal-title"><?php the_sub_field("list-title"); ?></h3>
                            <p class="internal-description"><?php the_sub_field("list-description"); ?></p>
                        <?php endif; ?>
                        </li>

                    <?php endwhile; endif; ?>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</main>