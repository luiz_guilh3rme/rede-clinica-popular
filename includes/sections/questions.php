<section class="questions">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                
                <?php echo do_shortcode("[contact-form-7 id='257' title='Formulário - Pergunte ao Doutor']"); ?>
                <!-- [contact-form-7 id="257" title="Formulário - Pergunte ao Doutor"] -->
            </div>
            <div class="col-lg col-12">
                <img src="<?php bloginfo("template_url")?>/img/questions/doctor-persona.png" alt="Persona Doutor" title="Persona Doutor" class="img-questions" aria-hidden="true" />
            </div>
        </div>
    </div>
</section>