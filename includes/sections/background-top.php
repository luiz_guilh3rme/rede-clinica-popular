
<?php

    $image = get_field('bg-top');



    if(!empty($image)):

        $size = 'background-topo';

        $thumb = $image['sizes'][ $size ];

    else:

        $thumb = "https://via.placeholder.com/1920x768";

    endif;   



    if(TEMPLATE_NAME != PAGE_SPECIALITIES):

?>

<section class="internal-img" style="background-image: url(<?= $thumb; ?>">

    <p class="internal-title text-uppercase"><?php the_field("text-1"); ?></p>

    <h2 class="internal-description"><?php the_field("text-2"); ?></h2>    

</section>



    <?php else: ?>



<section class="internal-img page-speciality" style="background-image: url(<?= $thumb; ?>">

    <h1 class="internal-title text-uppercase"><?php the_field("text-1"); ?></h1>

    <p class="internal-description"><?php the_field("text-2"); ?></p>    

</section>



    <?php endif; ?>