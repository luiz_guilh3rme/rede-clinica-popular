<?php 
    get_template_part("includes/sections/background-top"); 

    if(TEMPLATE_NAME != PAGE_SPECIALITIES):
        get_template_part("includes/sections/content-internal-general"); 
    else:
        get_template_part("includes/sections/content-internal-specialities"); 
    endif;